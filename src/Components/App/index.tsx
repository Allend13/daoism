import React from 'react'
import { ToastContainer } from 'react-toastify'

import Profile from '../Profile'
import TransferETH from '../TransferETH'
import Mint from '../Mint'
import {
  ContainerStyled, Layout, PaperStyled, ProfileStyled, TransferStyled, MintStyled,
} from './styled'

function App() {
  return (
    <>
      <ContainerStyled maxWidth="md">
        <Layout>
          <ProfileStyled>
            <PaperStyled>
              <Profile />
            </PaperStyled>
          </ProfileStyled>
          <TransferStyled>
            <TransferETH />
          </TransferStyled>
          <MintStyled>
            <Mint />
          </MintStyled>
        </Layout>
      </ContainerStyled>
      <ToastContainer />
    </>
  )
}

export default App
