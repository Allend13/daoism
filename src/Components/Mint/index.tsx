import React from 'react'

import { GridStyled, HeaderStyled } from './styled'

function Mint() {
  return (
    <GridStyled container direction="column">
      <HeaderStyled variant="h6" align="left">MINT:</HeaderStyled>
    </GridStyled>
  )
}

export default Mint
