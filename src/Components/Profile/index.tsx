import React from 'react'
import {
  useEthers,
} from '@usedapp/core'

import AccountNotConnected from './AccountNotConnected'
import AccountMeta from './AccountMeta'

function Profile() {
  const { account } = useEthers()

  if (!account) {
    return (
      <AccountNotConnected />
    )
  }

  return (
    <AccountMeta />
  )
}

export default Profile
