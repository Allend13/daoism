import React from 'react'
import {
  useEthers,
} from '@usedapp/core'
import { Button, CircularProgress, Grid } from '@mui/material'

import { GridStyled, HeaderStyled } from './styled'

function AccountMeta() {
  const { activateBrowserWallet } = useEthers()

  const handleActivate = () => activateBrowserWallet()

  return (
    <GridStyled container direction="column" alignItems="center">
      <HeaderStyled variant="h6" align="center">Connect wallet:</HeaderStyled>
      <Grid container flexGrow={1} alignItems="center" justifyContent="center">
        <Button variant="outlined" onClick={handleActivate}>Connect account</Button>
      </Grid>
    </GridStyled>
  )
}

export default AccountMeta
